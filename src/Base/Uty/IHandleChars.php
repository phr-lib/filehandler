<?php

namespace Phr\Filehandler\Base\Uty;

interface IHandleChars
{
    public const DOT = ".";

    public const CONFIGEXT = 'config';

    public const BREAK = "\n";

    public const BEA = "=";

    public const BEAX = "===";

    public const HEADERSTART = "==//";

    # FILE INTERNAL IDENTIFIER
    public const CONFIG = "CONFIG";

    public const CONFIGXS = "CONFxS";

    public const HBR = "&";

    public const DELIMINATER = "::";

    public const SIGNATURE = "signature/==";

    public const PIPE = "||";

    public const STARTCONTENT = "content::start:||\n \n";

    public const ENDCONTENT = "\n||:content::end\n";

    public const CERTLINE = "########################################\n";

    public const CERTCOM = "##########################\n";

    public const CERTSTR = "# CERTIFICATE                 \n";

    public const SIGSTR = "FILE SIGNATURE";

    public const SIGS = "# ";

    public const FILE_ID = "FileId::";

    public const CRWSTART = ">>>";

    public const CRWEND = "<<<";

    public const CRWBRAKE = "<<<>>>";

    public const CRWSPACE = "<<<<<";

    public const SIGWITHKEY = "Sign with: ";

    public const ORGANIZATION = "org=";


}