<?php

namespace Phr\Filehandler\Base\Uty;

interface IFileVar
{   
    public const DOT = ".";

    # FILE EXTENTIONS
    public const CONFIG = "config";

    public const CERT = "cert";

    public const CONFIGLIST = "sxlist";

    # FILE INTERNAL IDENTIFIER
    public const F_BASIC_CONFIG = "CONFIG";

    public const F_SECURE_CONFIG = "CONFxS";

    public const F_BASIC_LIST = "LIStF";  
    
    public const F_SECURE_LIST = "LISTxS";    
    
    public const F_BASIC_FILE = "SECFILE";

    public const F_SECURE_FILE = "SECFILExS";


}