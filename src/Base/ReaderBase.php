<?php

namespace Phr\Filehandler\Base;

use Phr\Filehandler\Base\Uty\IHandleChars;
use Phr\Filehandler\Base\Uty\IFileVar;
use Phr\Filehandler\FileHandlerException;
use Phr\Filehandler\Subvention\PassKey;

/**
 * @abstract ReaderBase
 * @abstract FileHandlerBase
 * @see FileHandlerBase
 */
abstract class ReaderBase extends FileHandlerBase
{   
    protected static int $filesize;

    protected static string $headerVarFile;

    protected static $fileContent;

    protected static function readFileContents( string $_file_path )
    {
        self::$filePath = $_file_path;

        self::$fileContent = self::readAll();

        self::readFileCreateTimestamp();

        self::readFileSize();
    }
    
    /**
     * @method loadPassKey
     * @var PassKey
     * Loads passKey into object.!
     * 
     */
    protected static function loadPassKey( PassKey $_pass_key ): void
    {   
        self::$protected = true;

        /**
         * @var static @FileJandlerBase
         * @var PassKey $passKey
         */
        self::$passKey = $_pass_key;
    }

    /**
     * @method readAll
     * @return allFileContent!
     */
    protected static function readAll()
    {   
        return file_get_contents( self::$filePath );
    }

    /**
     * @method readHeader
     * @return bool
     * Reads header:
     * options.: CONFIG,CONFIGX
     */
    protected static function readHeader(): bool
    {   
        $FileContent = explode( IHandleChars::STARTCONTENT, self::$fileContent );

        $FileHeader = $FileContent[0];

        if(!$FileHeader) throw new FileHandlerException("No file header!");

        self::$fileContent = $FileContent[1];

        $Header = explode( IHandleChars::HEADERSTART , $FileHeader );

        self::$headerVarFile = $Header[0];
        
        $HeaderSignature = explode( IHandleChars::BEA, $Header[1] );
        
        self::decodeProtectedFile( $HeaderSignature[0] );

        return true;
    }
    protected static function readSignature()
    {
        $FileContent = explode( IHandleChars::SIGNATURE, self::$fileContent );

        $FileSignature = $FileContent[1];

        self::$fileContent = $FileContent[0];
    }

    /**
     * @access private
     * 
     * @method decodeProtectedFile
     * @return bool
     * @var string
     * @var headerSignature
     */
    private static function decodeProtectedFile( string $_header_signature ): bool
    {   
        switch ( self::$headerVarFile ) 
        {
            case IFileVar::F_BASIC_CONFIG: return self::decodeTimeProtectedFile( $_header_signature ); break;

            case IFileVar::F_SECURE_CONFIG: return self::decodeSignatureProtectedFile( $_header_signature ); break;

            case IFileVar::F_BASIC_LIST: return self::decodeTimeProtectedFile( $_header_signature ); break;

            case IFileVar::F_SECURE_LIST: return self::decodeTimeProtectedFile( $_header_signature ); break;

            case IFileVar::F_BASIC_FILE: return self::decodeTimeProtectedFile( $_header_signature ); break;

            default: throw new FileHandlerException("File variant invalid!"); break;
        }
    }
    private static function decodeTimeProtectedFile( string $_header_signature ): bool
    {
        if( md5(self::$timeStamp) !== $_header_signature ) throw new FileHandlerException("File singnature invalid!");

        return true;
    }
    private static function decodeSignatureProtectedFile( string $_header_signature ): bool
    {  
        if( md5(self::$timeStamp . self::$passKey->key ) !== $_header_signature ) throw new FileHandlerException("File singnature invalid!");

        return true;
    }
    private static function readFileCreateTimestamp(): void
    {
        self::$timeStamp = (int)filemtime( self::$filePath );
    }
    private static function readFileSize(): void
    {
        self::$filesize = (int)filesize( self::$filePath );
    }
}