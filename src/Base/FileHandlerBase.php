<?php

namespace Phr\Filehandler\Base;

use Phr\Filehandler\Subvention\PassKey;

/**
 * @abstract FileHandlerBase
 * 
 * Basic abstraction of file handler
 */
abstract class FileHandlerBase 
{   
    /**
     * @access protected
     * @var static
     * @var handler
     */
    protected static $handler;

    /**     
     * @var static
     * @var string
     * @var filePath 
     */
    protected static string $filePath;

    /**
     * @var int
     * @var timeStamp
     */
    protected static int $timeStamp;

    /**
     * @var bool
     * @var protected
     */
    protected static bool $protected = false;

    /**
     * @var PassKey
     * @var passKey
     */
    protected static PassKey $passKey;

    /**
     * @method extention
     * @return string
     * @var string ( file path )
     */
    protected static function extention( string $_file_path ): string
    {
        return pathinfo( $_file_path, PATHINFO_EXTENSION);
    }

    /**
     * @method closeFile
     * @return void
     * 
     * Close file !!!
     */
    protected static function closeFile(): void 
    {
        fclose( self::$handler );
    }
    
    /// CONSTRUCT ***

    public function __construct( string $_file_path )
    {
        self::$filePath = $_file_path;
    }

    
}