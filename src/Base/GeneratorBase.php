<?php

namespace Phr\Filehandler\Base;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;
use Phr\Filehandler\Signature\SignatureData;
use Phr\Filehandler\Subvention\PassKey;

/**
 * @abstract GeneratorBase
 * @abstract FileHandlerBase
 * 
 * @see FileHandlerBase
 * 
 * 
 */
abstract class GeneratorBase extends FileHandlerBase
{   
    
    /**
     * @access public
     * 
     * @method sign
     * @return void
     * @var PassKey
     */
    public function sign( PassKey $_pass_key ): void
    {   
        self::$protected = true;

        self::$passKey = $_pass_key;
    }

    /**
     * @method loadSignatureData
     * @return void
     * @var static
     * @var null|SignatureData
     * 
     * Loads signature date with pass key. Protected must me true;
     * 
     */
    public static function loadSignatureData( null|SignatureData $_signature_data ): void
    {   
        if($_signature_data !== null)
        {
            self::$signatureData = $_signature_data;

            self::$passKey = self::$signatureData->programData->passKey;

            self::$protected = true;
        }    
    }

    /**
     * @method name
     * @return void
     * @var string
     */
    public function name( string $_new_file_name ): void 
    {
        $this->filename = $_new_file_name;
    }

    /**
     * @access protected
     * 
     * @var static
     * @var string
     * @var fullFilePath
     */
    protected static string $fullFilePath;

    /**
     * @var fileId
     */
    protected static string $fileId;

    /**
     * @var null|SignatureData
     * @var signatureData
     */
    protected static null|SignatureData $signatureData = null;

    /**
     * @method openFile
     * @return void
     * @var string file
     * 
     * Opens file !!!
     */
    protected static function openFile( string $_file ): void 
    {   
        self::$fullFilePath = self::$filePath.DIRECTORY_SEPARATOR.$_file;

        self::$handler = fopen( self::$fullFilePath, "w" );
    }

    /**
     * @method fileHeader
     * @return void
     * @var string
     * @var fileVar
     * 
     * Reads file header. 
     * 
     */
    protected static function fileHeader( string $_file_var ): void
    {   
        $FileTimeControl = md5( self::$timeStamp );

        if( self::$protected == true)
            fwrite(self::$handler, $_file_var . IC::HEADERSTART . self::headerSignature() .IC::BEA  );
        else fwrite(self::$handler, $_file_var . IC::HEADERSTART . self::headerTimeSignature() .IC::BEA  );

        fwrite(self::$handler, IC::BREAK);
        fwrite(self::$handler, IC::BREAK);
    }

    /**
     * @method headerTimeSignature
     * @return string
     * 
     * Encoded file signature.
     * 
     */
    protected static function headerTimeSignature(): string
    {
        return md5( self::$timeStamp );
    }

    /**
     * @method headerSignature
     * @return string  double encoded file signature.
     */
    protected static function headerSignature(): string
    {
        return md5( self::$timeStamp . self::$passKey->key );
    }

    
    

    protected static function fileCertificate(): void
    {
        fwrite(self::$handler, IC::PIPE . IC::SIGNATURE . IC::CERTCOM );

        self::sigLine( self::$fileId  );
        if( self::$protected == true)
            self::sigLine( IC::SIGWITHKEY . self::$passKey->keyId  );

        self::sigLine( date("Y-M-d \T H:i:sp", self::$timeStamp ) );
        
        if( self::$signatureData != null )
        {
            self::sigLine( self::$signatureData->programData->toCert() );

            self::sigLine( self::$signatureData->programData->passKey->toCert() );

            self::sigLine( self::$signatureData->organizationData->toCert() );

        }

        fwrite(self::$handler, IC::CERTLINE);
    }
    /**      
     * @var string
     * @var filename
     * 
     * Default file name.
     * 
     */
    protected string $filename = "api";

    /// CONSTRUCT ***

    public function __construct( string $_file_path )
    {
        parent::__construct( $_file_path );

        self::$timeStamp = time();

        self::$fileId = self::generateUniqueKeyId( IC::FILE_ID );
    }

    /**
     * @access private
     * 
     * @method sigLine
     * @var string|int
     * @var singnatureText
     */
    private static function sigLine( string|int $_line_text ): void
    {
        fwrite(self::$handler, IC::SIGS . $_line_text . IC::BREAK );
    }
    private static function generateUniqueKeyId( string $_pfeFix = "", bool $_t = true ): string 
    {
        return uniqid($_pfeFix, $_t);
    }
}