<?php

namespace Phr\Filehandler\ConfigHandler;

use Phr\Filehandler\Base\Uty;
use Phr\Filehandler\Base\Uty\IHandleChars;
use Phr\Filehandler\Base\ReaderBase;
use Phr\Filehandler\Subvention\PassKey;

class ConfigReader extends ReaderBase
{   
    

    public static function read( null|PassKey $_option = null )
    {
        parent::readFileContents( self::$filePath );
        
        if( $_option != null )
            self::loadPassKey( $_option );

        parent::readHeader();

        parent::readSignature();

        $Content = explode( IHandleChars::CRWBRAKE, self::$fileContent);
        
        $Result = array();

        foreach($Content as $block)
        {   
            $ContentRow = explode( IHandleChars::BEAX, $block );
            
            if(!empty($ContentRow[0]) && !empty($ContentRow[1]))
            {   
                $CheckArraKey = trim($ContentRow[0]);
                $Result[$CheckArraKey] = $ContentRow[1];
            }

        } 
        return $Result;
    }
}