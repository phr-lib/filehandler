<?php

namespace Phr\Filehandler\ConfigHandler;

use Phr\Filehandler\Base\Uty;
use Phr\Filehandler\Base\Uty\IHandleChars;
use Phr\Filehandler\Base\ReaderBase;
use Phr\Filehandler\Subvention\PassKey;

/**
 * SecureListReader
 * @abstract ReaderBase
 * 
 * @see ReaderBase
 * 
 */
class SecureListReader extends ReaderBase
{   
    /**
     * @access public
     * 
     * @method read
     * @var null|PassKey
     * 
     */
    public static function read( null|PassKey $_option = null )
    {
        parent::readFileContents( self::$filePath );
        
        if( $_option != null )
            self::loadPassKey( $_option );

        parent::readHeader();

        parent::readSignature();

        $Content = explode( IHandleChars::CRWBRAKE, self::$fileContent);
        
        $Result = array();

        foreach($Content as $block)
        {   
            array_push( $Result, explode( IHandleChars::CRWEND, $block) );
        } 
        return $Result;
    }
}