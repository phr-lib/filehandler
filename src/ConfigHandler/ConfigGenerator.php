<?php

namespace Phr\Filehandler\ConfigHandler;

use Phr\Filehandler\Base\GeneratorBase;
use Phr\Filehandler\ConfigHandler\ConfigContent\ConfigContent;
use Phr\Filehandler\Base\Uty\IHandleChars as IC;
use Phr\Filehandler\Base\Uty\IFileVar as FV;
use Phr\Filehandler\Subvention\PassKey;

/**
 * ConfigGenerator
 * @abstract GeneratorBase
 * 
 * Config file generator
 * 
 */
class ConfigGenerator extends GeneratorBase 
{   
    
    /**
     * @method generate
     * @return void
     * @var ConfigContent
     * 
     * Generates cert file.
     * 
     */
    public function generate( ConfigContent $_config_content ): void
    {
        self::openFile( $this->filename.FV::DOT.FV::CONFIG );

        if( self::$protected == true)
            self::fileHeader( FV::F_SECURE_CONFIG );
        else self::fileHeader( FV::F_BASIC_CONFIG );

        fwrite(self::$handler, IC::STARTCONTENT );

        foreach( $_config_content::$configContentRows as $contentRow )
        {
            fwrite(self::$handler, $contentRow->print());
            fwrite(self::$handler, IC::BREAK);
        }

        self::fileCertificate();

        self::closeFile();
    }

    
    
}