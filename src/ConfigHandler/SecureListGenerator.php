<?php

namespace Phr\Filehandler\ConfigHandler;

use Phr\Filehandler\Base\GeneratorBase;
use Phr\Filehandler\ConfigHandler\ConfigContent\SecureList;
use Phr\Filehandler\Base\Uty\IHandleChars as IC;
use Phr\Filehandler\Base\Uty\IFileVar as FV;


class SecureListGenerator extends GeneratorBase
{
    /**
     * @method generate
     * @return void
     * @var ConfigContent
     * 
     * Generates cert file.
     * 
     */
    public function generate( SecureList $_secure_list ): void
    {
        self::openFile( $this->filename.FV::DOT.FV::CONFIGLIST );
        echo self::$protected;
        if( self::$protected == true)
            self::fileHeader( FV::F_SECURE_LIST );
        else self::fileHeader( FV::F_BASIC_LIST );

        fwrite(self::$handler, IC::STARTCONTENT );

        foreach( $_secure_list::$secureContentRows as $contentRow )
        {
            fwrite(self::$handler, $contentRow->print());
        }
        fwrite(self::$handler, IC::BREAK);

        self::fileCertificate();

        self::closeFile();
    }
}