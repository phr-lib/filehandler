<?php

namespace Phr\Filehandler\ConfigHandler\ConfigContent;

use Phr\Filehandler\ConfigHandler\ConfigContent\SecureListRow;

class SecureList 
{   
    public static array $secureContentRows = [];
    
    public function __construct( SecureListRow $_content_row )
    {
        array_push( self::$secureContentRows, $_content_row);
    }
    public function add( SecureListRow $_content_row  )
    {
        array_push( self::$secureContentRows, $_content_row);
    }
}