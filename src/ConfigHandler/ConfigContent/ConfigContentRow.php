<?php

namespace Phr\Filehandler\ConfigHandler\ConfigContent;

use Phr\FileHandler\Base\Uty\IHandleChars as IC;


class ConfigContentRow 
{   
    public string $key;

    public string $value; 

    public function __construct( string $_key, string|int $_value )
    {
        $this->key = $_key;

        $this->value = $_value;
    }

    public function print(): string 
    {
        return  $this->key
                .IC::BEAX
                .$this->value 
                .IC::CRWBRAKE
                .IC::BREAK;
    }
}