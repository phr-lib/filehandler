<?php

namespace Phr\Filehandler\ConfigHandler\ConfigContent;

use Phr\FileHandler\Base\Uty\IHandleChars as IC;


class SecureListRow 
{   
    public string $value; 

    public function __construct( string|int $_value )
    {
        $this->value = $_value;
    }

    public function print(): string 
    {
        return  $this->value 
                .IC::CRWEND
                .IC::BREAK;
    }
}