<?php

namespace Phr\Filehandler;

use Phr\Filehandler\ConfigHandler\ConfigContent\ConfigContent;
use Phr\Filehandler\Subvention\PassKey;
use Phr\Filehandler\Signature\SignatureData;
use Phr\Filehandler\ConfigHandler\ConfigContent\SecureList;


interface IFileHandler
{
    public const DEFAULT_CONFIGNAME = 'api';

    public const DEFAULT_SECURECONFIG = 'secureApi';

    public const DEFAULT_FILELISTNAME = 'list';

    public const DEFAULT_FILESECURENAME = 'contentSecure';

    /**
     * @static loadSignatureData
     * @var SignatureData
     */
    public static function loadSignatureData( SignatureData $_signature_data ): void;

    /**
     * @method read
     * @return content
     * @var object|array|string|int
     * 
     * @var PassKey for password secured files!
     */
    public static function read( null|PassKey $_option = null ): object|array|string|int;

    /**
     * @method generateSimpleConfig
     * 
     * @var ConfigContent
     * Generates simple config file!
     */
    public function generateSimpleConfig( ConfigContent $_config_content ): void;

    /**
     * @method generateProtectedConfig
     * @var ConfigContent
     * @var PassKey
     * Generate secure config file!
     */
    public function generateProtectedConfig( ConfigContent $_config_content, PassKey $_pass_key ): void;

    /**
     * @method generateList
     * @var SecureList
     * @var string fileName
     * Generate secure sign config file.
     */
    public function generateList( SecureList $_secure_list, string $_filename =  IFileHandler::DEFAULT_FILELISTNAME ): void;

    /**
     * @method generateSecureList
     * @var SecureList
     * @var string filename
     */
    public function generateSecureList( SecureList $_secure_list, string $_filename =  IFileHandler::DEFAULT_FILELISTNAME ): void;

    /**
     * @method generateSecureFile
     * @var string content
     * @var string filename
     */
    public function generateSecureFile( string $_content, string $_filename =  IFileHandler::DEFAULT_FILESECURENAME ): void;

}