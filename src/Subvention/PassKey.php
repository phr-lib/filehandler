<?php

namespace Phr\Filehandler\Subvention;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;

class PassKey 
{
    public string $keyId;

    public string $key;

    public function __construct( string $_keyId, string $_key )
    {
        $this->keyId = $_keyId;

        $this->key = $_key;
    }
    public function toCert(): string
    {
        return IC::CRWSPACE
            . IC::CRWSTART
            . $this->keyId 
            . IC::CRWSPACE
            . IC::CRWSTART 
            . $this->key 
            . IC::CRWEND;
    }
}