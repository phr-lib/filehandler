<?php

namespace Phr\Filehandler\SecureFile;

use Phr\Filehandler\Base\Uty;
use Phr\Filehandler\Subvention\PassKey;
use Phr\Filehandler\Base\ReaderBase;

class SecureFileReader extends ReaderBase
{   
    

    public static function read( null|PassKey $_option = null )
    {
        parent::readFileContents( self::$filePath );
        
        if( $_option != null )
            self::loadPassKey( $_option );

        parent::readHeader();

        parent::readSignature();

        $ExtractContent = explode( Uty\IHandleChars::PIPE, self::$fileContent );
        return $ExtractContent[0];
    }
}