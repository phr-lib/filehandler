<?php

namespace Phr\Filehandler\SecureFile;

use Phr\Filehandler\Base\GeneratorBase;
use Phr\Filehandler\ConfigHandler\ConfigContent\ConfigContent;
use Phr\Filehandler\Base\Uty\IHandleChars as IC;
use Phr\Filehandler\Base\Uty\IFileVar as FV;
use Phr\Filehandler\Subvention\PassKey;

/**
 * SecureFileGenerator
 * @abstract GeneratorBase
 * 
 * Secure file generator
 * 
 */
class SecureFileGenerator extends GeneratorBase 
{   
    
    /**
     * @method generate
     * @return void
     * @var content
     * 
     * Generates secure content file
     * 
     */
    public function generate( string $_content ): void
    {
        self::openFile( $this->filename.FV::DOT.FV::CERT );

        self::fileHeader( FV::F_BASIC_FILE );

        fwrite(self::$handler, IC::STARTCONTENT );

        fwrite(self::$handler, $_content );

        fwrite(self::$handler, IC::BREAK );

        self::fileCertificate();

        self::closeFile();
    }

    
    
}