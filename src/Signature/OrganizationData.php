<?php

namespace Phr\Filehandler\Signature;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;

class OrganizationData 
{
    public string $organisation;

    public string $organisationId;

    public string $organisationAdminEmail;

    public function __construct( 
        string $_organisation
        ,string $_organisation_id
        ,string|int $_organisation_admin_email 
        )
    {
        $this->organisation = strtoupper( preg_replace('/\s+/', '_', $_organisation) );
        
        $this->organisationId = $_organisation_id;

        $this->organisationAdminEmail = $_organisation_admin_email;
    }

    public function toCert(): string
    {
        return IC::CRWSPACE
            . IC::CRWSTART
            . $this->organisation 
            . IC::CRWSPACE
            . IC::CRWSTART 
            . $this->organisationId 
            . IC::CRWEND
            . IC::CRWSTART
            . $this->organisationAdminEmail
            . IC::CRWEND  
            . IC::CRWSPACE;

    }
}