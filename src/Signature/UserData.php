<?php

namespace Phr\Filehandler\Signature;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;

class UserData 
{
    public string $userId;

    public string $userEmail;

    public string $firstName;

    public string $lastName;

    public string $userIDE_no;

    public function __construct( 
        string $_user_id
        ,string $_user_email
        ,string $_first_name 
        ,string $_last_name 
        ,string $_user_IDE_number 
        )
    {
        $this->userId = $_user_id;
        
        $this->userEmail = $_user_email;

        $this->firstName = $_first_name;

        $this->lastName = $_last_name;

        $this->userIDE_no = $_user_IDE_number;
    }

    public function toCert(): string
    {
        return IC::CRWSPACE
            . IC::CRWSTART
            . $this->programName 
            . IC::CRWSPACE
            . IC::CRWSTART 
            . $this->version 
            . IC::CRWEND
            . IC::CRWSPACE;

    }
}