<?php

namespace Phr\Filehandler\Signature;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;

class SignatureData 
{
    public ProgramData $programData;

    public null|OrganizationData $organizationData;

    public null|UserData $userData;

    public function __construct( 
        ProgramData $_program_data
        ,null|OrganizationData $_organization_data = null
        ,null|UserData $_user_data = null
        )
    {
        $this->programData = $_program_data;

        $this->organizationData = $_organization_data;

        $this->userData = $_user_data;
    }
}