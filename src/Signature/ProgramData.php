<?php

namespace Phr\Filehandler\Signature;

use Phr\Filehandler\Base\Uty\IHandleChars as IC;
use Phr\Filehandler\Subvention\PassKey;

class ProgramData 
{
    public string $programName;

    public string $version;

    public string|int $installtionId;

    public PassKey $passKey;

    public function __construct( 
        string $_program_name
        ,string $_version
        ,string|int $_installation_id 
        ,PassKey $_pass_key
        )
    {
        $this->programName = preg_replace('/\s+/', '_', $_program_name);
        
        $this->version = $_version;

        $this->installtionId = $_installation_id;

        $this->passKey = $_pass_key;
    }

    public function toCert(): string
    {
        return IC::CRWSTART
            . $this->programName
            . IC::CRWEND 
            . IC::CRWSPACE
            . IC::CRWSPACE
            . IC::CRWSTART 
            . $this->version 
            . IC::CRWEND
            . IC::CRWSPACE
            . IC::CRWSPACE
            . IC::CRWSPACE;

    }
}