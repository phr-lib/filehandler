<?php

namespace Phr\Filehandler;

use Phr\Filehandler\ConfigHandler\ConfigContent\ConfigContent;
use Phr\Filehandler\ConfigHandler\ConfigContent\ConfigContentRow;
use Phr\Filehandler\Base\Uty\IFileVar;
use Phr\Filehandler\ConfigHandler\ConfigReader;
use Phr\Filehandler\ConfigHandler\SecureListReader;
use Phr\Filehandler\Subvention\PassKey;
use Phr\Filehandler\ConfigHandler\ConfigContent\SecureList;
use Phr\Filehandler\Signature\SignatureData;
use Phr\Filehandler\SecureFile;

/**
 * FileHandler
 * @abstract FileHandlerBase
 * 
 * @see IFileHandler
 * 
 */
class FileHandler extends Base\FileHandlerBase implements IFileHandler
{   
    private static null|SignatureData $signatureData = null;

    public static function loadSignatureData( SignatureData $_signature_data ): void
    {
        self::$signatureData = $_signature_data;
    }
    public function generateSimpleConfig( ConfigContent $_config_content, string $_filename =  IFileHandler::DEFAULT_CONFIGNAME ): void
    {   
        $ConfigFile = new ConfigHandler\ConfigGenerator( self::$filePath );

        $ConfigFile->name( $_filename );

        $ConfigFile->generate( $_config_content );
    }
    public function generateList( SecureList $_secure_list, string $_filename =  IFileHandler::DEFAULT_FILELISTNAME ): void
    {   
        $SecureListFile = new ConfigHandler\SecureListGenerator( self::$filePath );

        $SecureListFile->name( $_filename );

        $SecureListFile->generate( $_secure_list );
    }
    public function generateSecureList( SecureList $_secure_list, string $_filename =  IFileHandler::DEFAULT_FILELISTNAME ): void
    {   
        $SecureListFile = new ConfigHandler\SecureListGenerator( self::$filePath );

        $SecureListFile->name( $_filename );

        $SecureListFile->loadSignatureData( self::$signatureData );

        $SecureListFile->generate( $_secure_list );
    }
    public function generateProtectedConfig( ConfigContent $_config_content, PassKey $_pass_key, string $_filename =  IFileHandler::DEFAULT_SECURECONFIG ): void
    {   
        $ConfigFile = new ConfigHandler\ConfigGenerator( self::$filePath );

        $ConfigFile->sign( $_pass_key );

        $ConfigFile->name( $_filename );

        $ConfigFile->generate( $_config_content );
    }
    public function generateSecureFile( string $_content, string $_filename =  IFileHandler::DEFAULT_FILESECURENAME ): void
    {   
        $SecureContentFile = new SecureFile\SecureFileGenerator( self::$filePath );

        $SecureContentFile->name( $_filename );

        $SecureContentFile->generate( $_content );
    }
    public static function read( null|PassKey $_option = null ): object|array|string|int
    {   
        $FileName = array_slice( explode( DIRECTORY_SEPARATOR,self::$filePath ), -1);

        switch ( parent::extention( $FileName[0] ) ) {

            case IFileVar::CONFIG: return ConfigHandler\ConfigReader::read( $_option ); break;

            case IFileVar::CERT: return SecureFile\SecureFileReader::read( $_option ); break;

            case IFileVar::CONFIGLIST: return ConfigHandler\SecureListReader::read( $_option ); break;

            default: throw new FileHandlerException("can not defile file extention!"); break;
        }
    }
}